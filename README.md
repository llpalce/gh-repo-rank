# Dear guys

There was some struggle, but finally i did it:)

I recognized that main focus for application is carefull data management. Gathering data from api takes a lot of time.
That is why i decided to store results locally in case od network interrupts during download, etc.

Obviously i wanted to add option to choose another community, but ran out of time, Although it cann be easily implemented - all redux structure is ready for that.

I hope you find my code interesting and give some feedback - impatiently waiting for that.

I've commited .env file just for your comfort - know shuldn't do that for real projects.







In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.